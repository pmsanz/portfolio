from PIL import Image, ImageFilter
import csv
import os, os.path
import pandas as pd
import shutil


#funcion de prueba
def test(argv):
    return print(argv)

#Donde ocurre toda la magia
def imageprepare(argv):
    """
    This function returns the pixel values.
    The imput is a png file location.
    """
    im = Image.open(argv).convert('L')
    width = float(im.size[0])
    height = float(im.size[1])
    newImage = Image.new('L', (28, 28), (0))  # creates white canvas of 28x28 pixels

    if width > height:  # check which dimension is bigger
        # Width is bigger. Width becomes 20 pixels.
        nheight = int(round((20.0 / width * height), 0))  # resize height according to ratio width
        if (nheight == 0):  # rare case but minimum is 1 pixel
            nheight = 1
            # resize and sharpen
        img = im.resize((20, nheight), Image.ANTIALIAS).filter(ImageFilter.SHARPEN)
        wtop = int(round(((28 - nheight) / 2), 0))  # calculate horizontal position
        newImage.paste(img, (4, wtop))  # paste resized image on white canvas
    else:
        # Height is bigger. Heigth becomes 20 pixels.
        nwidth = int(round((20.0 / height * width), 0))  # resize width according to ratio height
        if (nwidth == 0):  # rare case but minimum is 1 pixel
            nwidth = 1
            # resize and sharpen
        img = im.resize((nwidth, 20), Image.ANTIALIAS).filter(ImageFilter.SHARPEN)
        wleft = int(round(((28 - nwidth) / 2), 0))  # caculate vertical pozition
        newImage.paste(img, (wleft, 4))  # paste resized image on white canvas

    # newImage.save("sample.png

    tv = list(newImage.getdata())  # get pixel values

    # normalize pixels to 0 and 1. 0 is pure white, 1 is pure black.
    #tva = [(255 - x) * 1.0 / 255.0 for x in tv]
#    print(tv)
    return tv

#Le paso 1 path de imagen y me retorna su array de pixeles
def imageToPixel(pathImagen):
    
    valid_images = ['.jpg'] 
    ext = os.path.splitext(pathImagen)[1]
    if ext in valid_images :
        x=[imageprepare(pathImagen)]
        return x
    

#Paso un directorio y me retorna un DataFrame con los datos
def imagesToPixel(spathIN):
    imgs = []
    path = spathIN

    valid_images = ['.jpg'] 
    for f in os.listdir(path):  
        ext = os.path.splitext(f)[1]
        if ext in valid_images :
            imgs.append(os.path.join(path,f))   

    simple_list=[]
    for img in imgs:    
        simple_list.append(imageprepare(img)) #file path here

    
    x = pd.DataFrame(simple_list)

  
    return x

#Le doy un directorio de imagenes y un path para guardar, y me retorna un CSV con todas las imagenes, en pixeles
def imagesToCSV(spathIN,spathOUT):
    imgs = []
    path = spathIN

    valid_images = ['.jpg'] 
    for f in os.listdir(path):  
        ext = os.path.splitext(f)[1]
        if ext in valid_images :
            imgs.append(os.path.join(path,f))   

    for img in imgs:    
        x=[imageprepare(img)]#file path here
        with open(spathOUT, 'a', newline='') as f:
            writer = csv.writer(f,quoting=csv.QUOTE_NONNUMERIC)
            writer.writerow(x[0])

def imagesToCSVLabeled(spathIN,spathOUT,sLabel):
    imgs = []
    
    if os.path.exists(spathOUT) == False:
        pathPlantilla = "C:/Repositorio/IA/MNIST/MNIST Handwrite/input/TRAIN_PLANTILLA.csv"
        shutil.copy(pathPlantilla, spathOUT)
    path = spathIN

    valid_images = ['.jpg'] 
    for f in os.listdir(path):  
        ext = os.path.splitext(f)[1]
        if ext in valid_images :
            imgs.append(os.path.join(path,f))   
    
    list_lines = []
    
    for img in imgs:
        line = [sLabel]
        line.extend(imageprepare(img))
        list_lines.append(line)
        
#    for s in list_x:   
#        for digit in s:
#            list_lines.append(str(sLabel) + "," + digit[1:-1])

#    for row in csv.reader(str(list_lines)):
#        print(row)
#        break
#    print(list_lines)
          
    with open(spathOUT, 'a', newline='') as f:
        writer = csv.writer(f,quoting=csv.QUOTE_NONNUMERIC)
        writer.writerows(list_lines)
            

        