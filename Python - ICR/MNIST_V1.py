
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import lib.model as md
from sklearn.model_selection import ShuffleSplit
from sklearn.metrics import confusion_matrix
from keras.utils.np_utils import to_categorical
import os, os.path
import shutil 
import time
import IMGtoMNIST as imx
from sklearn.externals import joblib 


#test = x_test[0].reshape(-1, 28, 28, 1)
#result = model.predict(x_myTest)

#Desde aca esta el organizador

def CargarUltimaMNIST(sPath):
#    model = md.load_model('Digits-1.3.0.h5')
    if (os.path.exists(sPath)):
        print("CargarUltimaMNIST- El modelo existe")
        model = md.load_model(sPath)
    else:
        model = md.load_model('Digits-1.3.0.h5')
    print("modelo cargado satisfactoriamente")
    return model


def Entrenar(sTrain,sTest):
 
    start_time = time.time()

    # Load the data
    train = pd.read_csv(sTrain)
   
  
    print(train.shape)
    
    #Prepare dataset
    y = train["label"]
   
      
    X = train.drop("label", axis = 1)
    print(y.value_counts().to_dict())
    y = to_categorical(y, num_classes = 10)
    del train
    
    X = X / 255.0
    X = X.values.reshape(-1,28,28,1)
    
    # Shuffle Split Train and Test from original dataset
    seed=2
    train_index, valid_index = ShuffleSplit(n_splits=1,
                                            train_size=0.9,
                                            test_size=None,
                                            random_state=seed).split(X).__next__()
    x_train = X[train_index]
    Y_train = y[train_index]
    x_test = X[valid_index]
    Y_test = y[valid_index]
    
    # Parameters
    epochs = 30
    batch_size = 64
#    batch_size = 5
    validation_steps = 40
    
    # initialize Model, Annealer and Datagen
    model, annealer, datagen = md.init_model()
    
    # Start training
    train_generator = datagen.flow(x_train, Y_train, batch_size=batch_size)
    test_generator = datagen.flow(x_test, Y_test, batch_size=batch_size)
    
    history = model.fit_generator(train_generator,
                        steps_per_epoch=x_train.shape[0]//batch_size,
                        epochs=epochs,
                        validation_data=test_generator,
                        validation_steps=validation_steps//batch_size,
                        callbacks=[annealer])
    
    score = model.evaluate(x_test, Y_test)
    print('Test accuracy: ', score[1])
    
    # Saving Model for future API
    model.save('Digits-1.3.0.h5')
    print("Saved model to disk")
    
    #Load Model, correr desde aca
#    model = md.load_model('Digits-1.3.0.h5')
    
    # summarize history for accuracy
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='lower right')
    plt.show()
    
    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper right')
    plt.show()
    
    ## Model predict
    
    test = pd.read_csv(sTest)
    print(test.shape)
    test = test / 255
    test = test.values.reshape(-1, 28, 28, 1)
    p = np.argmax(model.predict(test), axis=1)
    
    print('Base model scores:')
    valid_loss, valid_acc = model.evaluate(x_test, Y_test, verbose=0)
    valid_p = np.argmax(model.predict(x_test), axis=1)
    target = np.argmax(Y_test, axis=1)
    cm = confusion_matrix(target, valid_p)
    print(cm)
    
    ## Preparing file for submission
    submission = pd.DataFrame(pd.Series(range(1, p.shape[0] + 1), name='ImageId'))
    submission['Label'] = p
    filename="keras-cnn-{0}.csv".format(str(int(score[1]*10000)))
    submission.to_csv(filename, index=False)
    
    elapsed_time = time.time() - start_time
    print("Elapsed time: {0}".format(time.strftime("%H:%M:%S", time.gmtime(elapsed_time))))
       
        
    
    return model


def moveFiles(sIn,sOut,sCategorical):
 
    
    shutil.move(sIn, sOut + sCategorical)  
    
    
    return True

def ImgIsNumber(sIn):
    
    model_saved = joblib.load('C:/Repositorio/IA/MNIST/MNIST Handwrite/models/binary_v2.job')
    
    result = model_saved.predict(imx.imagesToPixel(sIn))
    
    if result > 0:
        return True
    else:
        return False
    
def PixelIsNumber(pIn):
    
    model_saved = joblib.load('C:/Repositorio/IA/MNIST/MNIST Handwrite/models/binary_v2.job')
    
    result = model_saved.predict(pIn)
    
    return result
    

def returnCategorical(result,percent):
    
    if result[0] > percent:
        return 0
    if result[1] > percent:
        return 1
    if result[2] > percent:
        return 2
    if result[3] > percent:
        return 3
    if result[4] > percent:
        return 4
    if result[5] > percent:
        return 5
    if result[6] > percent:
        return 6
    if result[7] > percent:
        return 7
    if result[8] > percent:
        return 8
    if result[9] > percent:
        return 9
    else:
        return -1

def Clasificar(sPathIN,sPathOUT,percent):
    imgs = []
    
#    Filtro por imagenes validas
    valid_images = ['.jpg']
    for f in os.listdir(sPathIN):
        ext = os.path.splitext(f)[1]
        if ext in valid_images :
            imgs.append(os.path.join(sPathIN,f))   
    

    simple_list=[]
    for img in imgs:    
        simple_list.append(imx.ImageToPixel((img))) #file path here
    
    myTest = pd.read_csv("./input/MyTest.csv")
    columns = myTest.columns  
    x_myTest=pd.DataFrame(simple_list,columns=columns)
    
#   1ro muevo las imagenes que no son numeros
    son_numeros = PixelIsNumber(x_myTest)
 
    for n in range(len(son_numeros)):
        if son_numeros[n] == 0:
            moveFiles(imgs[n],str(-1))
        
#   Ajusto para predecir los numeros
    x_myTest = x_myTest / 255.0
    x_myTest = x_myTest.values.reshape(-1,28,28,1)
    
    model = CargarUltimaMNIST()
    
    result = model.predict(x_myTest)
    
    categorical = '1'
#   Muevo las imagenes segun reconociemiento
    
    for i in range(len(result)):
        categorical = returnCategorical(result[i],0.7)
        moveFiles(imgs[i],sPathOUT,str(categorical))
       
        
def Predecir(iPixel):  

    iPixel = iPixel / 255.0
    iPixel = iPixel.values.reshape(-1,28,28,1)
    
    model = CargarUltimaMNIST("C:/Repositorio/IA/MNIST/MNIST Handwrite/Digits-1.3.0.h5")
#    print(iPixel)
    return model.predict(iPixel)


if __name__ == '__main__':
    CargarUltimaMNIST("C:/Repositorio/IA/MNIST/MNIST Handwrite/Digits-1.3.0.h5")
    result = Predecir(imx.imagesToPixel("C:/Repositorio/IA/MNIST/MNIST Handwrite/extracts/Numeros"))
    cadena = ""
    for i in result:
            cadena= cadena + str(returnCategorical(i,0.7))
    print(cadena)





